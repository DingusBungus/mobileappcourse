$(document).ready(function(){
    var episodeList;
    $.ajax({
        url: "https://oq88q01z9j.execute-api.us-east-1.amazonaws.com/prod/yu-anime-record" ,
        dataType: "json",
        async: true,
        success: function (result) {
          /*left off here
          record parsing works successfully below
          still need to build a function which takes each record, adds it to a ui table and retrieves the image from s3
          */
          parseRecords(result.records);
        },
        error: function (request,error) {
            alert('Network error has occurred please try again!');
        }
    });
});

function parseRecords(episodeList){
  $.each(episodeList, function(idx, obj){
        var dyn_img = "http://yu-app.s3.amazonaws.com/test/" + obj.imageKey;
        $("#anime-list").append("<div class='col-lg-3 col-md-4 col-xs-6 thumb'><a class='thumbnail' href='recordDetail.html?uuId=" + obj.uuId + "'><img class='img-responsive' src='"+dyn_img+"' alt=''></a></div>");
    });
}
